<!DOCTYPE html>
<html>
    <head>
        <title>Jejokamp</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/fontawesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/header.css">
        <link rel="stylesheet" type="text/css" href="css/footer.css">
    </head>
    <body>
        <header class="mb-5">
            <nav class="navbar navbar-light navbar-expand-md justify-content-md-center justify-content-start">
                <button class="navbar-toggler mr-2" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
                    <span class="navbar-toggler-icon"></span>
                </button> 
                <a class="navbar-brand d-md-none d-inline" href="">Jejokamp</a>
                <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
                    <ul class="navbar-nav mx-auto text-md-center text-left">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Over Jejokamp</a> 
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Leiding</a>
                        </li>
                        <li class="nav-item my-auto">
                            <a class="nav-link navbar-brand mx-0 d-none d-md-inline" href="">Jejokamp</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Inschrijven</a> 
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Inschrijvingen</a> 
                        </li>
                    </ul>
                    <ul class="nav navbar-nav flex-row justify-content-md-center justify-content-start flex-nowrap">
                        <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-facebook mr-1"></i></a> </li>
                        <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-twitter"></i></a> </li>
                    </ul>
                </div>
            </nav>
        </header>