<?php include_once("header.php")?>
<head>
    <link rel="stylesheet" href="css/index.css">
</head>

<div class="container-fluid p-4">
    <div class="row">
        <div class="col-md-6 col-xl-3 p-3">
            <div class="information-article p-2">
                <i class="d-inline fa fa-info-circle mr-2"></i>
                <h2 class="d-inline">Over Jejokamp</h2>
                <hr>
                <p>
                    Nieuwsgierig naar wat Jejokamp inhoud? Op de <a href="">'Over Jejokamp'</a>-pagina wordt uitgelegd wat het Jejokamp inhoud,
                    op welke datum het kamp plaatsvindt en wat andere leeftijdsgenoten van het kamp vinden. Kortom, alle 
                    algemene informatie is terug te vinden op deze pagina!
                </p>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 p-3">
            <div class="information-article p-2">
                <i class="d-inline fa fa-users mr-2"></i>
                <h2 class="d-inline">Leiding</h2>
                <hr>
                <p>
                    Wil je weten wie er meegaan als leiding? Check dan de <a href="">leiding pagina</a>! Ieder lid van de leiding heeft een leuk
                    stukje over zichzelf geschreven, zodat je ze alvast een beetje leert kennen voor de kampweek!
                </p>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 p-3">
            <div class="information-article p-2">
                <i class="d-inline fa fa-id-badge mr-2"></i>
                <h2 class="d-inline">Inschrijven</h2>
                <hr>
                <p>
                    Ga mee op Jejokamp! Schrijf je in via <a href="">deze pagina</a>. Volg de instructies op de pagina en je bent binnen no-time opgegeven voor het kamp.
                    We zien je hopelijk tijdens de kampweek!
                </p>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 p-3">
            <div class="information-article p-2">
                <i class="d-inline fa fa-list mr-2"></i>
                <h2 class="d-inline">Inschrijvingen</h2>
                <hr>
                <p>
                    Ben jij ook super nieuwsgierig wie er allemaal meegaan op kamp? Of wil je even checken of je bent ingeschreven?
                    Dat kan via de <a href="">'Inschrijvingen'</a>-pagina. Hierop is te zien welke vriendjes of vriendinnetjes allemaal meegaan op kamp.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 d-flex justify-content-center" id="about-img">
            <img src="img/index/about.svg" alt="">
        </div>
    </div>
</div>

<div class="container mb-5 p-5">
    <h2 class="text-center">
        Nieuws
    </h2>
    <hr class="news-hr">
    <div id="carousel-news" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel-news" data-slide-to="0" class="active"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-10 p-3">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>    
</div>

<div class="container-fluid questions-background text-white p-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2 class="text-center">Vragen?</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12">
            <p class="text-center">Als u nog vragen heeft, neem dan contact op via een van de onderstaande kanalen!</p>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-2 text-center p-2">
            <i class="fa fa-envelope questions-icon"></i>
        </div>
        <div class="col-2 text-center p-2">
            <i class="fa fa-facebook questions-icon"></i>
        </div>
        <div class="col-2 text-center p-2">
            <i class="fa fa-instagram questions-icon"></i>
        </div>
    </div>
</div>
<?php include_once("footer.php")?>