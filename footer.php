        <footer class="footer-background pl-4 pr-4 pt-5 pb-4 text-white">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4" id="contactgegevens">
                        <h3>
                            Contact
                        </h3>
                        <ul>
                            <li>
                                Email: jejokamp@live.nl
                            </li>
                            <li>
                                Telefoon: 0616060161
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4" id="social-media">
                        <h3>
                            Social media
                        </h3>
                        <div>
                            <i class="fa fa-instagram mr-2 icon" style="color: white;"></i>
                            <i class="fa fa-facebook icon" style="color: white;"></i>
                        </div>
                        <p>
                            Volg ons op sociale media voor updates en leuke foto's!
                        </p>
                    </div>
                    <div class="col-md-4" id="tijd-tot-jejokamp">
                        <h3>
                            Tijd tot Jejokamp
                        </h3>
                        <p id="countdown"></p>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/popper.js"></script>
<script type="text/javascript">
    var countDownDate = new Date("Aug 9, 2020 12:00:00").getTime();
    var x = setInterval(function() {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    document.getElementById("countdown").innerHTML = "Tijd tot Jejokamp 2020: " +  days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "Verlopen";
    }
}, 1000);
</script>